var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCss = require('gulp-clean-css');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var rename = require('gulp-rename');
var hb = require('gulp-hb');
var sftp = require('gulp-sftp');

var paths = {
    sass: './_src/scss/*.scss',
    js: './_src/js/*.js',
    partials: './_src/hbs/partials/**/*.hbs',
    helpers: './_src/hbs/helpers/*.js',
    data: './_src/hbs/data/**/*.{js,json}',
    hbs: './_src/hbs/*.html',
    dest: './dist'
}

var scripts = function(minify, done) {
    gulp.src(paths.js)
        .pipe(gulpif(minify,uglify()))
        .pipe(gulpif(minify,concat('app.min.js'),concat('app.js')))
        .pipe(gulp.dest(paths.dest+'/js/'))
        .on('end', done);
};

gulp.task('dev-js', function(done) {
  return scripts(false, done);
});

gulp.task('prod-js', function(done) {
  return scripts(true, done);
});

gulp.task('scripts', function(production, done) {
    gulp.src(paths.js)
        .pipe(concat('app.js'))
        .pipe(gulp.dest(paths.dest+'/js'))
        .pipe(gulpif(production, minify))
        .pipe(gulp.dest(paths.dest+'/js'))
        .on('end', done);
});

gulp.task('sass', function(done) {
  gulp.src(paths.sass)
    .pipe(sass())
    .on('error', sass.logError)
    .pipe(gulp.dest(paths.dest+'/css/'))
    .pipe(minifyCss())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest(paths.dest+'/css/'))
    .on('end', done);
});

gulp.task('upload', function () {
	return gulp.src('dist/**/*')
		.pipe(sftp({
			host: 'lunenburg.dreamhost.com',
			auth: 'poetKey',
            remotePath: '/home/silastippens/poeti.codes/battleship/'
		}));
});


 
// Basic 
 
gulp.task('handlebars', function (done) {
    var hbStream = hb()
        .partials(paths.partials)
        .helpers(paths.helpers)
        .data(paths.data)
        .data({
            test: [
                'one',
                'two'
            ]
        });
        
    gulp
        .src(paths.hbs)
        .pipe(hbStream)
        .pipe(gulp.dest(paths.dest))
        .on('end', done);
});

gulp.task('watch', function() {
  gulp.watch(paths.sass, ['sass', 'upload']);
  gulp.watch(paths.js, ['dev-js', 'upload']);
  gulp.watch([paths.hbs, paths.partials, paths.data, paths.helpers], ['handlebars', 'upload']);
});