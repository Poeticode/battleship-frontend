(function() {
    var stuff = $('.box');
    
   // ANIMATION
    var masterTimeline = new TimelineMax({ paused: false, repeat: -1});
    var timelineCnt = 0;
    stuff.each(function(index) {
        timelineCnt = timelineCnt + 0.001;
        masterTimeline.add(
            TweenLite.fromTo(this, 1, {'backgroundColor': '#224499'}, {'backgroundColor': '#113366'})
        , timelineCnt);
        masterTimeline.add(
            TweenLite.fromTo(this, 1, {'backgroundColor': '#113366'}, {'backgroundColor': '#224499'})
        , timelineCnt+2);              
    });  
   
    
    var shipWrapper = $("<div style='width: 10vw; height: 2vw'></div>");
    var maxX = $('table.board').data('maxX');
    var maxY = $('table.board').data('maxY');
    var height = $(document).height()*0.02;
    var width = ($(document).width()*0.02)*5;

    // Create the SVG
    var shipSVG = new Image();
    shipSVG.src = "./imgs/grey_ship.svg";
    //shipWrapper.append(shipSVG);

    // Set up the ship
    $('.board').after(shipWrapper);
    $(shipWrapper).attr('id', 'ship_large'); 
    $(shipWrapper).attr('class', 'ship');
    $(shipWrapper.find('img')[0]).css({'width':'100%'});
    $(shipWrapper).data('length', 5);
    
    //Draggable.create("#ship_large");

    // Set up the Draggable variables
    var thresHold = "0%",
        ships = $(".ship"),
        boxes = $(".box"),
        boxOffsets = [];

    boxes.hover(function hovering() {
        var $this = $(this);
        var box_x = $this.data('location-x');
        var box_y = $this.data('location-y');
        
        if (box_y != null) {
            if (box_y == "a") {
                $('tr').eq(0).find('th').eq(box_x).css({'border-bottom':'2px solid #991111'});
            } else {
                $('.box[data-location-x="'+box_x+'"][data-location-y="'+prevChar(box_y)+'"]').css({'border-bottom':'2px solid #991111'});
            }
            if (box_x == 1) {
                $('tr').eq(charToIndex(box_y)+1).find('td').eq(box_x-1).css({'border-right':'2px solid #991111'});
            } else {
                $('.box[data-location-x="'+(box_x-1)+'"][data-location-y="'+box_y+'"]').css({'border-right':'2px solid #991111'});
            }
            
            if (box_x != maxX) {
                $('.box[data-location-x="'+(box_x+1)+'"][data-location-y="'+box_y+'"]').css({'border-left':'2px solid #991111'});
            }
            
            if (box_y != maxY) {
                $('.box[data-location-x="'+box_x+'"][data-location-y="'+nextChar(box_y)+'"]').css({'border-top':'2px solid #991111'});
            }
        }
    }, function undoHovering() {
        var $this = $(this);
        var box_x = $this.data('location-x');
        var box_y = $this.data('location-y');
        
        if (box_y != null) {
            if (box_y == "a") {
                $('tr').eq(0).find('th').eq(box_x).css({'border-bottom':''});
            } else {
                $('.box[data-location-x="'+box_x+'"][data-location-y="'+prevChar(box_y)+'"]').css({'border-bottom':''});
            }

            if (box_x == 1) {
                $('tr').eq(charToIndex(box_y)+1).find('td').eq(box_x-1).css({'border-right':''});
            } else {
                $('.box[data-location-x="'+(box_x-1)+'"][data-location-y="'+box_y+'"]').css({'border-right':''});
            }

            if (box_x != maxX) {
                $('.box[data-location-x="'+(box_x+1)+'"][data-location-y="'+box_y+'"]').css({'border-left':''});
            }
            
            if (box_y != maxY) {
                $('.box[data-location-x="'+box_x+'"][data-location-y="'+nextChar(box_y)+'"]').css({'border-top':''});
            }
        }
    });
    // Save the offset of all ze boxes
    $.each(boxes, function(index, el) {
        boxOffsets.push($(el).offset());
    });

    //set up the original offset for the ships
    $.each(ships, function(i,e) {
        e.originalOffset = $(e).offset();
    });

    // Begin creating the Draggable shit!
    Draggable.create(".ship", {
        type:"x,y",
        onDrag: function dragging() {
            for (var i = 0; i < boxes.length; i++) {
                if (this.hitTest(boxes[i], thresHold)) {
                    $(boxes[i]).addClass("highlight");
                } else {
                    $(boxes[i]).removeClass("highlight");
                }
            }
        },        
        onDragEnd: function dragEndCallback(event) {
            var i = boxes.length,
                snappedEl = false,
                snappedBoxOffsets = [],
                shipOffset = $(event.currentTarget).offset();
                
            // If they somehow weren't hovering over an element with their mouse
            // Just pretend the mouse was at the top left corner.
            // This results in the algorithm choosing the top-left block.
            if (!shipOffset) shipOffset = {left: 0, top: 0};
            
            // Check to see what boxes we're overlapping
            for (var cnt = 0; cnt < i; cnt++) {
                if (this.hitTest(boxes[cnt], thresHold)) {
                    snappedBoxOffsets.push(boxOffsets[cnt]);
                    snappedEl = true;
                }
            }
            var originalOffset = this.target.originalOffset;
            var targetOffset = {left: Infinity, top: Infinity, distance: Infinity};
            var currentOffset;

            for (var cnt = 0; cnt < snappedBoxOffsets.length; cnt++) {
                currentOffset = snappedBoxOffsets[cnt];

                // use pythagoreamrean theroerem
                var pythagA = currentOffset.left - shipOffset.left;
                var pythagB = currentOffset.top - shipOffset.top;
                var pythagC = Math.sqrt( pythagA*pythagA + pythagB*pythagB );

                // Update the new target if it's closer than the previous target
                if (pythagC < targetOffset.distance) {
                    targetOffset.left = currentOffset.left;
                    targetOffset.top = currentOffset.top;
                    targetOffset.distance = pythagC;
                } 


            }

            // If they didn't hover over a box, send ship back to inventory
            if ((!snappedEl) || (targetOffset.left === Infinity)) {
                TweenLite.to(this.target, 0.2, {x:0, y:0});
            } else {
                $(this.target).addClass("snapped");                    
                TweenLite.to(this.target, 0.1, {
                    x: targetOffset.left - originalOffset.left,
                    y: targetOffset.top - originalOffset.top
                });
            }
        }
        
    });
    
    function nextChar(c) {
        return String.fromCharCode(c.charCodeAt(0) + 1);
    }
    function prevChar(c) {
        return String.fromCharCode(c.charCodeAt(0) - 1);
    }
    function charToIndex(c) {
        return c.charCodeAt(0) - 97;
    }

})();

